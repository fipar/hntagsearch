from bs4 import BeautifulSoup
import requests

#List of keywords to search on hn articles
KEYWORDS=['vpn','data','dbms','ddos','mysql','postgresql','relational','vim','emacs','sql','nosql']


def get_page(url="https://news.ycombinator.com/news"):
    url = url
    r = requests.get(url)
    page = BeautifulSoup(r.text,features="html.parser")
    return page

def look_for_keyword(page,word_list=None):
    for article in page.find_all('a', class_='storylink'):
        for word in word_list:
            if word.lower() in article.get_text().lower():
                return ("%s (%s)" % (article.get_text(), article['href']) )

if __name__ == '__main__':
    page = get_page()
    print (look_for_keyword(page,KEYWORDS))
